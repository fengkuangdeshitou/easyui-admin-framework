﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using App.Core.Domain.BaseObject;
using System.ComponentModel;
using App.Core.Domain.Auditing;

namespace App.Core.Domain
{
    public class Permission : IEntity
    {
        public Guid Id { get; set; }

        public virtual Guid MenuId { get; set; }
    }

    public class RolePermission : Permission
    {
        public virtual Guid RoleId { get; set; }
    }

    public class UserPermission : Permission
    {
        public virtual Guid UserId { get; set; }
    }

    /// <summary>
    /// 用户
    /// </summary>
    public class User : FullAuditedEntity
    {
        [StringLength(ColumnSetting.Short)]
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        [StringLength(ColumnSetting.Longer)]
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        [StringLength(ColumnSetting.Phone)]
        public string Mobile { get; set; }
        public bool MobileConfirmed { get; set; }
        [ForeignKey("UserId")]
        public virtual ICollection<Role> Roles { get; set; }
        [ForeignKey("UserId")]
        public virtual ICollection<UserPermission> Permissions { get; set; }
        public virtual Team Team { get; set; }
        public virtual ICollection<LoginLog> LoginLogs { get; set; }
        [StringLength(ColumnSetting.Long)]
        public string OpenId { get; set; }
    }

    /// <summary>
    /// 角色
    /// </summary>
    public class Role : FullAuditedEntity
    {
        [StringLength(ColumnSetting.Short)]
        public string RoleName { get; set; }
        [StringLength(ColumnSetting.Remark)]
        public string RoleDesc { get; set; }
        [ForeignKey("UserId")]
        public virtual ICollection<User> Users { get; set; }
        [ForeignKey("RoleId")]
        public virtual ICollection<RolePermission> Permissions { get; set; }
    }

    /// <summary>
    /// 组织架构
    /// </summary>
    public class Team: FullAuditedEntity
    {
        public Guid? ParentId { get; set; }
        [StringLength(ColumnSetting.Short)]
        public string TeamName { get; set; }
        [StringLength(ColumnSetting.Remark)]
        public string TeamDesc { get; set; }
        public int SortOrder { get; set; }
        [ForeignKey("ParentId")]
        public virtual ICollection<Team> ChildTeams { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }

    /// <summary>
    /// 登录日志
    /// </summary>
    public class LoginLog: IEntity,IHasCreationTime
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(ColumnSetting.Short)]
        public string IpAddress { get; set; }
        public virtual User User { get; set; }
        public DateTime? CreationTime { get; set; }
    }

    /// <summary>
    /// 日程事件
    /// </summary>
    public class CalendarEvent : FullAuditedEntity
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [StringLength(ColumnSetting.Short)]
        public string Color { get; set; }
        [StringLength(ColumnSetting.Remark)]
        public string Subject { get; set; }
        public virtual User User { get; set; }
    }
}
