﻿using App.Core.Domain.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Core.Domain.BaseObject
{
    public class BaseModel : BaseModel<Guid>
    {

    }

    public class BaseModel<TPrimaryKey> :IEntity<TPrimaryKey>, IAudited
    {
        [Key]
        public TPrimaryKey Id { get; set; }
        public Guid? CreatorUserId { get; set; }
        public DateTime? CreationTime { get; set; }
        public Guid? LastModifierUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        [Timestamp]
        public byte[] TimeStamp { get; set; }
    }
}
