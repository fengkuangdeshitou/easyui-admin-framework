﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Domain.BaseObject
{
    public interface IEntity<TPrimaryKey>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        TPrimaryKey Id { get; set; }
    }

    public interface IEntity : IEntity<Guid>
    {

    }
}
