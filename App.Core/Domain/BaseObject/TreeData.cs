﻿using System;
using System.Collections.Generic;

namespace App.Core.Domain.BaseObject
{
    public class TreeData<T>
    {
        public Guid? id { get; set; }

        public string text { get; set; }

        public string iconCls { get; set; }

        public bool leaf { get; set; }

        public bool expanded { get; set; }

        public IList<T> children { get; set; }
    }
}
