﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using App.Core.Domain.BaseObject;
using App.Core.Domain.Auditing;

namespace App.Core.Domain
{
    /// <summary>
    /// 系统设置
    /// </summary>
    public class Setting : FullAuditedEntity
    {
        [StringLength(ColumnSetting.Short)]
        public string Key { get; set; }
        [StringLength(ColumnSetting.Longer)]
        public string Value { get; set; }
        [StringLength(ColumnSetting.Remark)]
        public string Desc { get; set; }
        public bool IsHidden { get; set; }
        public bool IsSystem { get; set; }
    }

    /// <summary>
    /// 功能权限
    /// </summary>
    public class MenuPermission : FullAuditedEntity
    {
        public Guid? ParentId { get; set; }
        [StringLength(30)]
        public string MenuText { get; set; }
        [StringLength(256)]
        public string MenuLink { get; set; }
        public int SortOrder { get; set; }
        [StringLength(10)]
        public string MenuType { get; set; }
        public string IconCls { get; set; }
        [StringLength(10)]
        public string IsActivated { get; set; }
        public bool AutoCreated { get; set; }
        [ForeignKey("ParentId")]
        public virtual ICollection<MenuPermission> ChildMenuPermissions { get; set; }
    }

    /// <summary>
    /// 按钮
    /// </summary>
    public class Button : FullAuditedEntity
    {
        [StringLength(ColumnSetting.Short)]
        public string ButtonName { get; set; }
        [StringLength(ColumnSetting.Remark)]
        public string ButtonDesc { get; set; }
        [StringLength(ColumnSetting.Url)]
        public string ButtonLink { get; set; }
        [StringLength(ColumnSetting.Short)]
        public string IconCls { get; set; }
    }

    public class Seo:IEntity {
        [Key]
        public Guid Id { get; set; }
        public string H1 { get; set; }
        public string Title { get; set; }
        public string Keyowrds { get; set; }
        public string Description { get; set; }
    }

    public class Dictionary : FullAuditedEntity {
        [StringLength(ColumnSetting.VeryShort)]
        public string Key { get; set; }
        [StringLength(ColumnSetting.Short)]
        public string Desc { get; set; }
        [StringLength(ColumnSetting.Longer)]
        public string Dictionarys { get; set; }
    }
}
