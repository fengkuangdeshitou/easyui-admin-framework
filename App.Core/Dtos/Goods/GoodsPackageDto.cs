﻿using System.Collections.Generic;

namespace App.Core.Dtos
{
    public partial class GoodsPackageDto : BulkEntityDto    {
        public string PackageName { get; set; }
        public string Photo { get; set; }
        public decimal Price { get; set; }
		public string Description { get; set; }
        public string CustomRedeemCode { get; set; }

        public List<GoodsDto> Goods { get; set; }
    }
}