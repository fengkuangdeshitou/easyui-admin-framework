﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.Core.Dtos
{
    public partial class ButtonDto: BulkEntityDto
    {
        [Required]
        public string ButtonName { get; set; }
        public string ButtonDesc { get; set; }
        [Required]
        public string ButtonLink { get; set; }
        [Required]
        public string IconCls { get; set; }
    }

    public partial class CalendarEventDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Color { get; set; }
        [Required]
        [MaxLength(500)]
        public string Title { get; set; }
    }

    public partial class MenuPermissionDto
    {
        public Guid? ParentId { get; set; }
        [Required]
        public string MenuText { get; set; }
        public string MenuLink { get; set; }
        public string MenuType { get; set; }
        public string IconCls { get; set; }
        public int SortOrder { get; set; }
        public string IsActivated { get; set; }
    }

    public partial class RoleDto : BulkEntityDto
    {
        [Required]
        public string RoleName { get; set; }
        public string RoleDesc { get; set; }
    }

    public partial class SettingDto : BulkEntityDto
    {
        [Required]
        public string Key { get; set; }
        [Required]
        public string Value { get; set; }
        [Required]
        public string Desc { get; set; }
    }
}
