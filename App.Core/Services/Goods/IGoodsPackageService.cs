﻿using System.Threading.Tasks;
using App.Core.Domain.BaseObject;
using App.Core.Dtos;
using App.Core.Domain;
using System;
using System.Collections.Generic;
using App.Core.Dtos.Goods;

namespace App.Services
{
    public partial interface IGoodsPackageService
    {
        /// <summary>
        /// 批量保存
        /// </summary>
        /// <param name="bulks"></param>
        /// <returns></returns>
        Task<OperationResult> SaveGoodsPackages(BulkDto<GoodsPackageDto> dtos);

        /// <summary>
        /// 获取商品套装
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<GoodsPackageDto> GetGoodsPackageAsync(Guid id);

        GoodsPackageDto GetGoodsPackage(Guid id);

        GridResult<RedeemCodeDto> GetRedeemCodes(int page, int rows, string sort = "r.CreationTime", string order = "asc", Guid? packageId = null, string filterRules = "");

        /// <summary>
        /// 关联套餐商品
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="linkGoods"></param>
        /// <returns></returns>
        Task<OperationResult> LinkPackageGoods(Guid packageId,List<Guid> linkGoods);

        /// <summary>
        /// 生成兑换码
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="createNumber"></param>
        /// <returns></returns>
        Task<OperationResult> CreateRedeemCodes(Guid packageId, int createNumber);

        /// <summary>
        /// 清空兑换码
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        Task<OperationResult> EmptyRedeemCodes(Guid packageId);

        /// <summary>
        /// 获取兑换码
        /// </summary>
        /// <param name="redeemCodeSN"></param>
        /// <returns></returns>
        Task<RedeemCode> GetRedeemCodeBySNAsync(string redeemCodeSN);

        /// <summary>
        /// 根据订单ID获取兑换码
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        Task<List<RedeemCode>> GetRedeemCodesByOrderId(Guid orderId);

        /// <summary>
        /// 移除关联商品
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        Task<OperationResult> RemoveLinkPackageGoods(Guid packageId, Guid[] guid);

        Task<OperationResult> SavePackageDescription(Guid id, string description);
    }
}
