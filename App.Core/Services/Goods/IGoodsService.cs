﻿using App.Core.Domain;
using App.Core.Domain.BaseObject;
using App.Core.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace App.Services
{
    public partial interface IGoodsService : IDependency
    {
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> CreateGoods(GoodsDto dto);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> UpdateGoods(Guid id, GoodsDto dto);

        /// <summary>
        /// 保存配置
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> SaveBrands(BulkDto<BrandDto> dto);

        /// <summary>
        /// 获取商品信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Goods> GetGoods(Guid id);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="idList"></param>
        /// <returns></returns>
        Task DeleteAsync(Guid[] idList);

        #region GoodsCategory
        /// <summary>
        /// 获取分类
        /// </summary>
        /// <returns></returns>
        List<GoodsCategory> GetCategorys(Expression<Func<GoodsCategory, bool>> predicate = null);

        /// <summary>
        /// 创建分类
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> CreateCategory(GoodsCategoryDto dto);

        /// <summary>
        /// 修改分类
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> UpdateCategory(Guid id, GoodsCategoryDto dto);

        /// <summary>
        /// 删除分类
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteCategoryAsync(Guid id);
        #endregion
    }
}
