﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Domain.BaseObject;
using App.Core.Dtos;
using App.Core.Helpers;
using App.Core;
using App.Core.DataAccess;
using System.Data.Entity;
using App.Core.Domain;
using Omu.ValueInjecter;
using System.Linq.Expressions;
using EntityFramework.Extensions;

namespace App.Services.Impls
{
    public partial class DictionaryServiceImpl : AppServiceBase, IDictionaryService
    {
        private readonly CurrentSession _curUser;
        public DictionaryServiceImpl()
        {
            _curUser = new CurrentSession();
        }
        
		        public async Task<OperationResult> SaveDictionarys(BulkDto<DictionaryDto> dto)
        {
            using (var db = new Db())
            {
                var curUser = new CurrentSession();
                var exists = new List<string>();
                //批量插入
                foreach (var row in dto.InsertedRows)
                {
					                    if (await db.Dictionary.CountAsync(b => b.Key == row.Key) > 0)
                    {
                        exists.Add(string.Format("名为【{0}】的字典已经存在！", row.Key));
                    }
                    else
                    {
					                        var entity = new Dictionary();
                        entity.InjectFrom<NotNullInjection>(row);
                        db.Dictionary.Add(entity);
                    }                }
                //批量更新
                foreach (var row in dto.UpdatedRows)
                {
                    var entity = await db.Dictionary.FindAsync(row.Id);
                    entity.InjectFrom(new NotNullInjection(), row);
                }

                //删除
                var deleteArray = dto.DeletedRows.Where(row => row.Id != null).Select(s => s.Id).ToArray();
                if (deleteArray.Length > 0)
                {
                    await db.Dictionary.Where(s => deleteArray.Contains(s.Id)).DeleteAsync();
                }

                await db.SaveChangesAsync();

                return new OperationResult() { success = true, message = dto.DeletedRows.Count > 0 ? "数据已经成功删除！" : exists.Count > 0 ? String.Join("<br />", exists) : "保存成功！" };
            }
        }
		
		public async Task<Dictionary> GetDictionaryAsync(Guid id)
        {
            using (var db = new Db())
            {
                return await db.Dictionary.SingleAsync(s=>s.Id == id);
            }
        }

        public List<Dictionary> GetAll()
        {
            using (var db = new Db())
            {
                return db.Dictionary.ToList();
            }
        }

        public Dictionary<string,int> GetDictionaryText(string key)
        {
            using (var db = new Db())
            {
                var entity = db.Dictionary.Single(s => s.Key == key);
                if (entity != null) {
                    var result = new Dictionary<string, int>();
                    int i = 0;
                    foreach (var item in entity.Dictionarys.Split(','))
                    {
                        result.Add(item, i);
                        i++;
                    }

                    return result;
                }

                return new Dictionary<string, int>();
            }
        }
    }
}
