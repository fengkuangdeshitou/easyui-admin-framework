﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using App.Core.Domain;
using App.Core.Domain.BaseObject;
using App.Core.Dtos;

namespace App.Services
{
    public partial interface IDictionaryService
    {
				/// <summary>
        /// 批量保存
        /// </summary>
        /// <param name="bulks"></param>
        /// <returns></returns>
        Task<OperationResult> SaveDictionarys(BulkDto<DictionaryDto> dtos);
		
		/// <summary>
        /// 异步获取
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Dictionary> GetDictionaryAsync(Guid id);

        /// <summary>
        /// 获取字典
        /// </summary>
        /// <param name="key"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        Dictionary<string,int> GetDictionaryText(string key);

        /// <summary>
        /// 获取所有字典
        /// </summary>
        /// <returns></returns>
        List<Dictionary> GetAll();
    }
}
