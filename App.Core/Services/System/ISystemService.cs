﻿using App.Core.Domain;
using App.Core.Domain.BaseObject;
using App.Core.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Services
{
    public partial interface ISystemService : IDependency
    {
        /// <summary>
        /// 保存角色
        /// </summary>
        /// <param name="bulks"></param>
        /// <returns></returns>
        Task<OperationResult<Role>> SaveRoles(BulkDto<RoleDto> bulks);
        /// <summary>
        /// 保存配置
        /// </summary>
        /// <param name="bulks"></param>
        /// <returns></returns>
        Task<OperationResult<Setting>> SaveSettings(BulkDto<SettingDto> bulks);
        /// <summary>
        /// 保存按钮
        /// </summary>
        /// <param name="bulks"></param>
        /// <returns></returns>
        Task<OperationResult<Button>> SaveButtons(BulkDto<ButtonDto> bulks);
        /// <summary>
        /// 获取所有按钮
        /// </summary>
        /// <returns></returns>
        List<Button> GetAllButton();
        /// <summary>
        /// 获取所有角色
        /// </summary>
        /// <returns></returns>
        List<Role> GetAllRole();
        /// <summary>
        /// 根据角色名称获取指定角色
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        Task<Role> GetRoleByNameAsync(string roleName);
        Role GetRoleByName(string roleName);

        /// <summary>
        /// 保存角色所对应的按钮
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        OperationResult SaveRoleButtons(Guid id, BulkKeyDto<Guid> dto);
        /// <summary>
        /// 保存用户所对应的按钮
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        OperationResult SaveUserButtons(Guid id, BulkKeyDto<Guid> dto);

        #region menus
        /// <summary>
        /// 创建菜单
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult<MenuPermission>> CreateMenu(MenuPermissionDto dto);

        /// <summary>
        /// 修改菜单
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> UpdateMenu(Guid id, MenuPermissionDto dto);
        /// <summary>
        /// 获取当前登录用户的菜单数据
        /// </summary>
        /// <returns></returns>
        List<MenuPermission> GetAllMenuByUser();
        /// <summary>
        /// 获取指定用户的所有权限
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<MenuPermission> GetAllMenuByUserId(Guid userId);
        /// <summary>
        /// 保存菜单对应按钮
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> SaveMenuButtons(Guid id, BulkKeyDto<Guid> dto);

        /// <summary>
        /// 删除日程
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteCalendarEventAsync(Guid id);
        #endregion

        #region calendar
        /// <summary>
        /// 根据时间范围获取日程数据
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        List<CalendarEvent> GetCalendarEvents(DateTime start, DateTime end);
        /// <summary>
        /// 更新日程
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> UpdateCalendarEvent(Guid id, CalendarEventDto dto);
        /// <summary>
        /// 创建日程
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> CreateCalendarEvent(CalendarEventDto dto);

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteMenuAsync(Guid id);
        #endregion
    }
}
