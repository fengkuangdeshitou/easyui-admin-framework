﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using App.Core.Domain;
using App.Core.Domain.BaseObject;
using App.Core.Dtos;

namespace App.Services
{
    public partial interface IUserService: IDependency
    {
        #region users
        /// <summary>
        /// 创建用户
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult<User>> CreateUserAsync(UserDto dto);
        OperationResult<User> CreateUser(UserDto dto);

        /// <summary>
        /// 修改用户
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> UpdateUser(Guid id, UserDto dto);

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> ChangeUserPassword(ChangePwdDto dto);

        /// <summary>
        /// 修改用户密码
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        Task<OperationResult> ChangeUserPassword(Guid id, string newPassword);

        Task<OperationResult> ChangeUserOpenId(Guid id, string openId);

        /// <summary>
        /// 创建登录日志
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<OperationResult> CreateLoginLog(Guid userId);

        /// <summary>
        /// 根据用户名获取用户信息
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        Task<User> GetUserByUserNameAsync(string userName);

        User GetUserByOpenId(string openId);

        /// <summary>
        /// 获取用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        User GetUserById(Guid userId);
        #endregion

        #region teams
        /// <summary>
        /// 获取团队记录
        /// </summary>
        /// <returns></returns>
        List<Team> GeTeams(Expression<Func<Team, bool>> predicate = null);

        /// <summary>
        /// 创建部门
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult<Team>> CreateTeam(TeamDto dto);

        /// <summary>
        /// 修改部门
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> UpdateTeam(Guid id, TeamDto dto);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        Task DeleteAsync(Guid[] guid);

        /// <summary>
        /// 删除组织机构
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteTeamAsync(Guid id);
        #endregion
    }
}
