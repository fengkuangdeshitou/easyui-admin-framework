﻿using App.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Common
{
    public class BillNumberBuilder
    {
        private static object locker = new object();

        public static string NextBillNumber(string prefix = "")
        {
            lock (locker)
            {
                using (var db = new Db()) {
                    string str = DateTime.Now.ToShortDateString();

                    DateTime time1 = Convert.ToDateTime(str + " 0:00:00");
                    DateTime time2 = Convert.ToDateTime(str + " 23:59:59");

                    var todayTotal = db.Order.Count(s => s.CreationTime >= time1 & s.CreationTime <= time2);

                    return prefix + DateTime.Now.ToString("yyMMddfff") + (todayTotal+1).ToString().PadLeft(5, '0');
                }
            }
        }
        // 防止创建类的实例
        private BillNumberBuilder() { }
    }
}
